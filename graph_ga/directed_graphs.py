"""
Module for holding general graph classes and functionality.
"""
from collections import OrderedDict
from typing import Any, Iterable

import networkx as nx


class NodeQueue:
    """
    Class for handeling functionality of the node queue and order of execution.

    :raises ValueError: If trying to get element from empty queue.
    """

    def __init__(self) -> None:
        self._queue = OrderedDict()

    def qsize(self) -> int:
        """
        Returns size of queue.

        :return: Number of elements in queue.
        """
        return len(self._queue)

    def empty(self) -> bool:
        """
        Checks if queue is empty.

        :return: True if empty, False otherwise
        """
        if self._queue:
            return False

        return True

    def get(self) -> Any:
        """
        Removes and returns first element of queue.

        :return: First element of queue
        """
        # If queue length is 0, raise error
        if not self._queue:
            raise ValueError("Tried to get element from empty queue")

        # Get oldest value in queue and return
        _, value = self._queue.popitem(last=False)
        return value

    def put(self, element: Any) -> None:
        """
        Puts element in queue.

        :param element: Element to place into the queue.
        """
        self._queue[id(element)] = element

    def is_in(self, element: Any) -> bool:
        """
        Checks if an element is in the queue.

        :param element: Element to check if in the queue.
        """
        if id(element) in self._queue:
            return True

        return False


class DAG:
    """
    Class that holds the computation graph and handles graph functionality.

    :raises ValueError: If a non-existent parent node is used to add a new node.
    :raises RuntimeError: Under the following conditions:
        - If graph has a cycle.
        - If trying to execute an empty node queue.
    """

    def __init__(self) -> None:
        self.graph = nx.DiGraph()
        self.root_node = None
        self.last_node_added = None
        self.node_queue = NodeQueue()

    def add(self, node: Any, parent_nodes: Iterable[Any] = None) -> None:
        """
        Add a node to the directed graph.

        :param node: A node to add to the directed graph.
        :param parent_node: Parent node(s) of the node being added, i.e. the
            node(s) that feeds this node, defaults to None, or connect to
            previously added node.
        :raises ValueError: If trying to add node to a parent node that doesn't
            exist in the graph.
        """
        self.graph.add_node(node)

        # If any parent node(s) not in graph, raise error
        if parent_nodes is not None:
            for parent_node in parent_nodes:
                if not self.graph.has_node(parent_node):
                    raise ValueError(
                        "Tried connecting this node to other non-existing node."
                    )
        # If given another node, connect them from other node, to newly created node
        if parent_nodes is not None:
            for parent_node in parent_nodes:
                self.graph.add_edge(parent_node, node)
        # If not given another node
        elif self.last_node_added is not None:
            self.graph.add_edge(self.last_node_added, node)
        # If no other node given and no previous node, add a root node
        else:
            self.root_node = node

        self.last_node_added = node

    def check_if_acyclic(self) -> None:
        """
        Checks that the graph is acyclic.

        :raises RuntimeError: If cycle is detected in the graph.
        """
        try:
            nx.algorithms.cycles.find_cycle(self.graph)
            raise RuntimeError(
                "Created graph is cyclic, please re-create graph."
            )
        except nx.NetworkXNoCycle:
            return

    def graph_to_queue(self) -> None:
        """
        Creates execution queue from the graph.
        """
        # Check if acyclic
        self.check_if_acyclic()

        if self.root_node is not None:
            next_nodes = list(self.graph.successors(self.root_node))
            self.node_queue.put(self.root_node)
            # Convert graph to queue of execution
            for next_node in next_nodes:
                # Get next node successors in next nodes
                for child in list(self.graph.successors(next_node)):
                    next_nodes.append(child)

                # If the node is not already in the queue, add next node on node
                # queue
                if not self.node_queue.is_in(next_node):
                    self.node_queue.put(next_node)

    def execute_queue(self) -> None:
        """
        Executes the node queue and refills the queue for another execution.

        :raises RuntimeError: If trying to execute an empty node queue.
        """
        # Check if empty, if so raise error
        if self.node_queue.empty():
            raise RuntimeError("Unable to execute empty node queue.")

        # Otherwise execute nodes in order
        while not self.node_queue.empty():
            node_to_execute = self.node_queue.get()
            node_to_execute.execute()

        # Recreate queue
        self.graph_to_queue()
