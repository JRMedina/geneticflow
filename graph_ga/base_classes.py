"""
Module for holding base classes for distributed genetic algorithms.
"""
from typing import Any, Generator

import ray


def raise_not_imp_error(function_name: str) -> None:
    """
    Takes a function name and raises an error stating that the function was
    not implemented.

    :param function_name: Name of the funtion not implemented.
    :raises NotImplementedError: With function name.
    """
    raise NotImplementedError(
        f"{function_name} function not implemented. Please implement."
    )


class Individual:
    """
    Base class for the genetic algorithm individuals. Must be sub-classed and
    methods overidden.
    """

    def __init__(self) -> None:
        self.fitness = 0.0

    def get_fitness(self) -> float:
        """
        Returns fitness of the individual.
        """
        return self.fitness


def to_iterator(obj_ids: Any) -> Generator[Any, None, None]:
    """
    Forces ray to return object ids into iterators for tqdm progress bar. Use as
    such:
    fit_promises = [
            individual.fit.remote() for individual in self.individuals
        ]

        # Have progress bar for return of promises
        for _ in tqdm(to_iterator(fit_promises)):
            pass

    :param obj_ids: ray obj_ids.
    :yield: Iterator for ray obj_ids.
    """
    while obj_ids:
        done, obj_ids = ray.wait(obj_ids)
        yield ray.get(done[0])


class Population:
    """
    Class for representing the genetic population.
    """

    def __init__(self, individual_class: callable) -> None:
        self.individuals = []
        self.individual_class = individual_class

    def generate_individual(self, *args, **kwargs):
        """
        Generates an individual member and adds it to the individuals list.
        """
        self.individuals.append(self.individual_class.remote(*args, **kwargs))
