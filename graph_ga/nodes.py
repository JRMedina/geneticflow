"""
Module that holds all base node classes for genetic algorithms.
"""
from typing import Any, Dict, Type, Union

from graph_ga.base_classes import Population


class Node:
    """
    Base class for all nodes.

    :raises NotImplementedError: If running the executed method without
        overridding it.
    """

    def __init__(self):
        self.executed = False

    def execute(self, parent_node: Any) -> None:
        """
        Raises error if not overridden.

        :raises NotImplementedError: If not overridden for a subclass.
        """
        raise NotImplementedError(
            "'execute' function must be implemented for a Node subclass."
        )


class Fit(Node):
    """
    Base node responsible for calculating the fitness of a population.
    """

    def __init__(
        self,
        fitness_algorithm: callable,
        cpu_req: int,
        gpu_req: int,
        resources_required: Dict[str, Union[float, int]],
    ) -> None:
        super().__init__()
        self.fit = fitness_algorithm
        self.population: Type[Population] = None
        self.cpu_req = cpu_req
        self.gpu_req = gpu_req
        self.resources_required = resources_required

    def execute(self, parent_node: Any) -> None:
        """
        Calculate fitness of population at the individual level.

        :param parent_node: Parent node of this node,
        """
        # TODO: Call fit function as ray remote function with req resources
        self.population = parent_node.population
