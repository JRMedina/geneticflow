# Genetic Flow

[![pipeline status](https://gitlab.com/JRMedina/graph_ga/badges/master/pipeline.svg)](https://gitlab.com/JRMedina/graph_ga/commits/master)
[![coverage report](https://gitlab.com/JRMedina/graph_ga/badges/master/coverage.svg)](https://gitlab.com/JRMedina/graph_ga/commits/master)

Distributed genetic algorithms using Ray and directed acyclic graphs.

- Free software: MIT license
- Documentation: https://graph-ga.readthedocs.io.

## Features

- Instantiate any object, define fit, cross, and mutate function, and graph_ga will evolve
- Built in algorithms for generation selection
- Easy to extend with new algorithms for generation selection
- Easy to extend with your own models

## Credits

This package was created with Cookiecutter* and the `JRMedina/python_cookie_cutter`* project template.
