# Description

\<Description of the feature\>

# User Problem/Value

\<What problem does the user have that this solves, or, how does this provide value for a user\>

# Assumptions

\<What assumptions does this feature have\>

# Not Doing

\<What is out of scope for this feature\>

# Acceptance Criteria

- [ ] Code tested and code coverage not decreased
- [ ] Tests pass
- [ ] User provided value or problem solved
- [ ] Code documented and formatted
