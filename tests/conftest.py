"""
File for creating pytest fixtures available to any pytest test function within this tests folder.
"""
import time

from typing import Any, Optional, Tuple

import ray
import pytest

from graph_ga.base_classes import Individual
from graph_ga.nodes import Node


@pytest.fixture(scope="session", autouse=True)
def ray_start_and_shutdown() -> None:
    """
    Startup and shutdown ray for testing.
    """
    # Initialize ray
    ray.init()

    # Check ray is initialized
    assert ray.is_initialized() is True

    yield

    # Shutdown ray
    ray.shutdown()

    # Check it's not initialized
    assert ray.is_initialized() is False


@ray.remote
class Simple(Individual):
    """
    Simple Individual sublass for testing purposes.
    """

    def __init__(self, x: float, y: float, z: float) -> None:
        """
        Simple init function that call's Individual init and defines attributes.
        """
        # Call Individual's init
        super().__init__()

        self.var_x = x
        self.var_y = y
        self.var_z = z

    def get_attributes(self) -> Tuple[float, float, float]:
        """
        Return attributes of Simple actor.

        :return: Tuple of attributes.
        """
        return self.var_x, self.var_y, self.var_z

    def fit(self):
        """
        Simple fitness function that returns the sum of the simple attributes
        and sleeps.
        """
        # Get fitness
        self.fitness = self.var_x + self.var_y + self.var_z

        # Sleep
        time.sleep(1.0)


@pytest.fixture()
def simple_actor() -> callable:
    """
    Yields the Simple class for testing.
    """
    # Yield actor class
    yield Simple


# Disable too-few-public-methods as this is just a mock class for testing
class MockNodeWithExecution(Node):  # pylint: disable=too-few-public-methods
    """
    Custom class to mock a node with an execution function.
    """

    def __init__(self):
        super().__init__()
        self.executed = False

    def execute(self, parent_node: Optional[Any] = None) -> None:
        self.executed = True
