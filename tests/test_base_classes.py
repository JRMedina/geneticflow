"""
Test module for testing graph_ga.base_actors.py
"""
import ray
import pytest

from hypothesis import given
import hypothesis.strategies as st

from graph_ga.base_classes import Population, raise_not_imp_error

# raise_not_imp_error


@given(function_name=st.text())
def test_raise_not_imp_error_pos(function_name: str) -> None:
    """
    Test that the raise_not_imp_error correctly raises NotImplementedError
    given a function name.

    :param function_name: Generated function name.
    """
    with pytest.raises(NotImplementedError) as err:
        raise_not_imp_error(function_name=function_name)

    assert function_name in str(err.value)


# Population
# Population.generate_individual


def test_generate_individual_args_pos(simple_actor: callable) -> None:
    """
    Test that Population can generate an individual using *args.

    :param simple_actor: Handle to simple ray actor.
    """
    pop = Population(individual_class=simple_actor)

    pop.generate_individual(1.0, 2.0, 3.0)

    assert len(pop.individuals) == 1
    simple_actor_handle = pop.individuals[0]
    assert isinstance(simple_actor_handle, ray.actor.ActorHandle)
    assert ray.get(simple_actor_handle.get_fitness.remote()) == 0
    assert ray.get(simple_actor_handle.get_attributes.remote()) == (
        1.0,
        2.0,
        3.0,
    )


def test_generate_individual_kwargs_pos(simple_actor: callable) -> None:
    """
    Test that Population can generate an individual using **kwargs.

    :param simple_actor: Handle to simple ray actor.
    """
    pop = Population(individual_class=simple_actor)

    pop.generate_individual(x=1.0, y=2.0, z=3.0)

    assert len(pop.individuals) == 1
    simple_actor_handle = pop.individuals[0]
    assert isinstance(simple_actor_handle, ray.actor.ActorHandle)
    assert ray.get(simple_actor_handle.get_fitness.remote()) == 0
    assert ray.get(simple_actor_handle.get_attributes.remote()) == (
        1.0,
        2.0,
        3.0,
    )
