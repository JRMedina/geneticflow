"""
Module for testing the nodes.py module.
"""
import pytest

from graph_ga.nodes import Node

# Node


def test_execute_not_imp_neg() -> None:
    """
    Test that if using the base Node class, that the execute function raises a
    NotImplementedError.
    """

    node = Node()

    with pytest.raises(NotImplementedError) as err:
        node.execute(None)

    assert "execute" in str(err.value)
