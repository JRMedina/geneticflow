"""
Module for testing the directed_graphs module.
"""
import pytest

from conftest import MockNodeWithExecution
from graph_ga.directed_graphs import DAG, NodeQueue
from graph_ga.nodes import Node


# NodeQueue


# NodeQueue.qsize


def test_qsize_empty_queue_pos() -> None:
    """
    Test that qsize method of NodeQueue returns 0 for empty queue.
    """
    # Create empty queue
    queue = NodeQueue()

    assert queue.qsize() == 0


def test_qsize_queue_pos() -> None:
    """
    Test that qsize method of NodeQueue returns queue size.
    """
    # Create queue
    queue = NodeQueue()
    queue.put("blah")

    assert queue.qsize() == 1


# NodeQueue.empty


def test_empty_queue_pos() -> None:
    """
    Test that empty method of NodeQueue returns True for empty queue.
    """
    # Create empty queue
    queue = NodeQueue()

    assert queue.empty()


def test_non_empty_queue_pos() -> None:
    """
    Test that empty method of NodeQueue returns False for non-empty queue.
    """
    # Create queue
    queue = NodeQueue()
    queue.put("blah")

    assert not queue.empty()


# NodeQueue.get


def test_get_one_item_pos() -> None:
    """
    Test that get method of NodeQueue returns only element.
    """
    # Create queue
    queue = NodeQueue()
    queue.put("blah")

    element = queue.get()
    assert element == "blah"


def test_get_multiple_items_pos() -> None:
    """
    Test that get method of NodeQueue returns oldest element among queue.
    """
    # Create queue
    queue = NodeQueue()
    queue.put("blah")
    queue.put("blah_young")

    element = queue.get()
    assert element == "blah"

    element = queue.get()
    assert element == "blah_young"


def test_get_empty_neg() -> None:
    """
    Test that get method of NodeQueue raises ValueError on empty queue.
    """
    # Create empty queue
    queue = NodeQueue()

    with pytest.raises(ValueError) as err:
        queue.get()

    assert "empty queue" in str(err.value)


# NodeQueue.put


def test_put_element_pos() -> None:
    """
    Test that put method of NodeQueue places element inside queue.
    """
    check_string = "blah"

    # Create queue
    queue = NodeQueue()
    queue.put(check_string)

    assert queue.qsize() == 1
    assert queue.is_in(check_string)
    element = queue.get()
    assert element == check_string


# NodeQueue.is_in


def test_is_in_true_pos() -> None:
    """
    Test that is_in method of NodeQueue returns True if given element is in the
    queue.
    """
    check_string = "blah"

    # Create queue
    queue = NodeQueue()
    queue.put(check_string)

    assert queue.is_in(check_string)


def test_is_in_false_pos() -> None:
    """
    Test that is_in method of NodeQueue returns False if given element is not
    in the queue.
    """
    # Create queue
    queue = NodeQueue()

    assert not queue.is_in("blah")


# DAG

# DAG.add


def test_add_root_node_pos() -> None:
    """
    Test that DAG class can succesfully add a root node.
    """
    test_dag = DAG()
    root_node = Node()

    # Add root node
    test_dag.add(root_node)

    assert test_dag.root_node == root_node


def test_add_non_root_node_pos() -> None:
    """
    Test that DAG class can succesfully add a node when another node node
    exists connecting it to the previously added node.
    """
    test_dag = DAG()
    root_node = Node()
    new_node = Node()

    # Add root node and new node
    test_dag.add(root_node)
    test_dag.add(new_node)

    assert test_dag.root_node == root_node
    assert test_dag.last_node_added == new_node
    assert test_dag.graph.number_of_nodes() == 2
    assert list(test_dag.graph.predecessors(new_node)) == [root_node]


def test_add_consecutive_nodes_pos() -> None:
    """
    Test that DAG class can succesfully add multiple nodes, chaining them
    together.
    """
    test_dag = DAG()
    node_1 = Node()
    node_2 = Node()
    node_3 = Node()

    # Add root node and new node
    test_dag.add(node_1)
    test_dag.add(node_2)
    test_dag.add(node_3)

    assert test_dag.root_node == node_1
    assert test_dag.last_node_added == node_3
    assert test_dag.graph.number_of_nodes() == 3
    assert list(test_dag.graph.predecessors(node_1)) == []
    assert list(test_dag.graph.predecessors(node_2)) == [node_1]
    assert list(test_dag.graph.predecessors(node_3)) == [node_2]


def test_add_multi_parents_pos() -> None:
    """
    Test that DAG class can succesfully add a node with multiple parents.
    """
    test_dag = DAG()
    node_1 = Node()
    node_2 = Node()
    node_3 = Node()
    multi_parent_node = Node()

    # Add root node and new node
    test_dag.add(node_1)
    test_dag.add(node_2, (test_dag.root_node,))
    test_dag.add(node_3, (test_dag.root_node,))
    test_dag.add(multi_parent_node, (node_2, node_3))

    assert test_dag.root_node == node_1
    assert test_dag.last_node_added == multi_parent_node
    assert test_dag.graph.number_of_nodes() == 4
    assert list(test_dag.graph.predecessors(node_1)) == []
    assert list(test_dag.graph.predecessors(node_2)) == [node_1]
    assert list(test_dag.graph.predecessors(node_3)) == [node_1]
    assert list(test_dag.graph.predecessors(multi_parent_node)) == [
        node_2,
        node_3,
    ]
    assert list(test_dag.graph.successors(node_1)) == [node_2, node_3]
    assert list(test_dag.graph.successors(node_2)) == [multi_parent_node]
    assert list(test_dag.graph.successors(node_3)) == [multi_parent_node]


def test_add_nodes_to_root_pos() -> None:
    """
    Test that DAG class can succesfully add multiple nodes to the root node.
    """
    test_dag = DAG()
    node_1 = Node()
    node_2 = Node()
    node_3 = Node()

    # Add root node and new node
    test_dag.add(node_1)
    test_dag.add(node_2, (test_dag.root_node,))
    test_dag.add(node_3, (test_dag.root_node,))

    assert test_dag.root_node == node_1
    assert test_dag.last_node_added == node_3
    assert test_dag.graph.number_of_nodes() == 3
    assert list(test_dag.graph.predecessors(node_1)) == []
    assert list(test_dag.graph.predecessors(node_2)) == [node_1]
    assert list(test_dag.graph.predecessors(node_3)) == [node_1]
    assert list(test_dag.graph.successors(node_1)) == [node_2, node_3]


def test_add_node_non_existent_parent_node_neg() -> None:
    """
    Test that DAG class will raise an error if trying to add a node with a
    parent node that's not in the graph.
    """
    test_dag = DAG()

    # Add root node
    test_dag.add(Node())

    # Try to add node with bad parent node
    with pytest.raises(ValueError) as err:
        test_dag.add(Node(), (Node(),))

    assert "non-existing" in str(err.value)


# DAG.check_if_acyclic


def test_check_if_acyclic_pos() -> None:
    """
    Test that DAG will not raise an error if check_if_acyclic finds no cycle in
    the graph.
    """
    # Create a tree without a cycle
    test_dag = DAG()
    node_2 = Node()
    test_dag.add(Node())
    test_dag.add(node_2)

    # Check no error is raised
    test_dag.check_if_acyclic()


def test_check_if_acyclic_raises_error_neg() -> None:
    """
    Test that DAG will raise an error if check_if_acyclic finds a cycle in the
    graph.
    """
    # Create a tree with a cycle
    test_dag = DAG()
    node_2 = Node()
    test_dag.add(Node())
    test_dag.add(node_2)
    test_dag.graph.add_edge(node_2, test_dag.root_node)

    # Check error is raised
    with pytest.raises(RuntimeError) as err:
        test_dag.check_if_acyclic()

    assert "cyclic" in str(err.value)


# DAG.graph_to_queue


def test_graph_to_queue_no_nodes() -> None:
    """
    Test that there are no nodes in the DAG queue if there are none in the
    graph.
    """
    # Create a tree with one branch of nodes
    test_dag = DAG()

    # Create graph queue
    test_dag.graph_to_queue()

    # Check queue properties
    assert test_dag.node_queue.qsize() == 0


def test_graph_to_queue_one_node() -> None:
    """
    Test converting a graph with one node to the queue works as
    expected with the one node in the queue.
    """
    # Create a tree with one node
    test_dag = DAG()
    root_node = Node()
    test_dag.add(root_node)

    # Create graph queue
    test_dag.graph_to_queue()

    # Check queue properties and priority
    assert test_dag.node_queue.qsize() == 1
    for removed_node in [root_node]:
        assert removed_node == test_dag.node_queue.get()


def test_graph_to_queue_one_branch() -> None:
    """
    Test converting a graph with one branch of nodes to the queue works as
    expected with branch nodes in queue.
    """
    # Create a tree with one branch of nodes
    test_dag = DAG()
    root_node = Node()
    node_2 = Node()
    node_3 = Node()
    test_dag.add(root_node)
    test_dag.add(node_2)
    test_dag.add(node_3)

    # Create graph queue
    test_dag.graph_to_queue()

    # Check queue properties and priority
    assert test_dag.node_queue.qsize() == 3
    for removed_node in [root_node, node_2, node_3]:
        assert removed_node == test_dag.node_queue.get()


def test_graph_to_queue_two_branches() -> None:
    """
    Test converting a graph with two branches of nodes to the queue works as
    expected with each branch node in the queue.
    """
    # Create a tree with two branches of nodes
    test_dag = DAG()
    root_node = Node()
    node_2 = Node()
    node_3 = Node()
    test_dag.add(root_node)
    test_dag.add(node_2, (root_node,))
    test_dag.add(node_3, (root_node,))

    # Create graph queue
    test_dag.graph_to_queue()

    # Check queue properties and priority
    assert test_dag.node_queue.qsize() == 3
    for removed_node in [root_node, node_2, node_3]:
        assert removed_node == test_dag.node_queue.get()


def test_graph_to_queue_merging_branches() -> None:
    """
    Test converting a graph with merging branches of nodes to the queue works as
    expected where merging branches have the merging node only appear once.
    """
    # Create a tree with merging branches of nodes
    test_dag = DAG()
    root_node = Node()
    node_2 = Node()
    node_3 = Node()
    node_4 = Node()
    test_dag.add(root_node)
    test_dag.add(node_2, (root_node,))
    test_dag.add(node_3, (root_node,))
    test_dag.add(node_4, (node_2, node_3))

    # Create graph queue
    test_dag.graph_to_queue()

    # Check queue properties and priority
    assert test_dag.node_queue.qsize() == 4
    for removed_node in [root_node, node_2, node_3, node_4]:
        assert removed_node == test_dag.node_queue.get()


# DAG.execute_queue


def test_execute_queue_one_node_pos() -> None:
    """
    Test that DAG correctly executes the nodes in it's queue.
    """
    # Create a tree with one mock Node
    test_dag = DAG()
    test_dag.add(MockNodeWithExecution())

    # Create graph queue
    test_dag.graph_to_queue()
    assert test_dag.node_queue.qsize() == 1

    # Execute DAG, and check node(s) were executed
    test_dag.execute_queue()
    while not test_dag.node_queue.empty():
        assert test_dag.node_queue.get().executed


def test_execute_queue_no_node_neg() -> None:
    """
    Test that DAG raises error when trying to execute an empty queue.
    """
    # Create a tree with one mock Node
    test_dag = DAG()

    with pytest.raises(RuntimeError) as err:
        test_dag.execute_queue()

    assert "empty node queue" in str(err.value)


def test_execute_queue_multiple_node_pos() -> None:
    """
    Test that DAG correctly executes the nodes in it's queue.
    """
    # Create a tree with one mock Node
    test_dag = DAG()
    for _ in range(10):
        test_dag.add(MockNodeWithExecution())

    # Create graph queue
    test_dag.graph_to_queue()
    assert test_dag.node_queue.qsize() == 10

    # Execute DAG, and check node(s) were executed
    test_dag.execute_queue()
    while not test_dag.node_queue.empty():
        assert test_dag.node_queue.get().executed
